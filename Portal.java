package observerZad2;


import java.util.Observable;

public class Portal extends Observable {

    public void notify(String message){
        setChanged();
        notifyObservers(message);
    }


}
