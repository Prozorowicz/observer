package observerZad2;


import java.util.Observer;


public class User implements Observer {
    private String imie;
    private int panicLevel;

    public User(String imie, int panicLevel) {
        this.imie = imie;
        this.panicLevel = panicLevel;
    }

    @Override
    public void update(java.util.Observable o, Object arg) {
        System.out.println(imie + " powiadomiony o: " + arg);
    }
}
