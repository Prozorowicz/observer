package observerZad2;

public class Main {
    public static void main(String[] args) {
        Portal portal = new Portal();
        portal.addObserver(new User("a",1));
        portal.addObserver(new User("b",2));
        portal.addObserver(new User("c",3));
        portal.notifyObservers("upa");
    }
}
